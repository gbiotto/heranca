public class TestaConta {
    public static void main(String[] args){

        ContaCorrente cc1 = new ContaCorrente(2,1,"Banco Aa", 100.00, 1000.00);
        System.out.println(cc1);

        System.out.println("O saldo da conta corrente é R$ " + cc1.getSaldo());

        ContaPoupanca p1 = new ContaPoupanca(3,3,"Banco Cc",50.00,20,0.05);
        System.out.println("O saldo da conta poupanca é R$ " + p1.getSaldo());

        ContaSalario cs1 = new ContaSalario(4,4,"Banco Dd",120.00,3);
        System.out.println("Número limite de saques disponível: " + cs1.numLimiteSaques);

    }
}

