public class ContaCorrente extends Conta{
    protected double chequeEspecial;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }

    @Override
    public String toString() {
        return "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }

    public double getSaldo(){
        return this.chequeEspecial + this.saldo;
    }

    @Override
    public void sacar(double saque) {
        if(saque <= (this.saldo+this.chequeEspecial)){
            this.saldo = this.saldo - saque;
        }
    }

    @Override
    public void depositar(double deposito) {
        this.saldo = this.saldo + deposito;
        }
}
