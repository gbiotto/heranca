public class ContaPoupanca extends Conta{

    private int dataAniversario;
    private double taxaJuros;

    public ContaPoupanca(int numero, int agencia, String banco, double saldo, int dataAniversario, double taxaJuros) {
        super(numero, agencia, banco, saldo);
        this.dataAniversario = dataAniversario;
        this.taxaJuros = taxaJuros;
    }

    public double getSaldo(){
        return this.saldo + this.taxaJuros*this.saldo;
    }

    public void sacar(double saque) {
        if (saque <= this.saldo){
            this.saldo = this.saldo - saque;
        }
        else
            System.out.println("Saldo indisponível!");
    }
}
