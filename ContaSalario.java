public class ContaSalario extends Conta{
    protected int numLimiteSaques;

    public ContaSalario(int numero, int agencia, String banco, double saldo, int numLimiteSaques) {
        super(numero, agencia, banco, saldo);
        this.numLimiteSaques = numLimiteSaques;
    }

    public double getSaldo(){
        return this.saldo;
    }

    @Override
    public void sacar(double saque) {
        if (numLimiteSaques >= 0 && saque <= this.saldo){
            this.numLimiteSaques = this.numLimiteSaques - 1;
            this.saldo = this.saldo - saque;
        }
    }
}
